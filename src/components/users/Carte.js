import React from "react";

import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import { useDispatch } from "react-redux";
import { removeUser } from "../../redux/reducers/userSlice";

const Carte = ({ user }) => {
  const dispatch = useDispatch();
  return (
    <Card className="me-3" style={{ width: "18rem" }}>
      <Card.Img variant="top" src="holder.js/100px180" />
      <Card.Body>
        <Card.Title>Nom complet : {user.fullName}</Card.Title>
        <Card.Text>
          email : {user.email} <br />
          adresse : {user.adress}
        </Card.Text>
        <Button
          variant="primary"
          onClick={() => {
            alert("Utilisateur supprimé");
            dispatch(removeUser(user.id));
          }}
        >
          Supprimer
        </Button>
      </Card.Body>
    </Card>
  );
};

export default Carte;
