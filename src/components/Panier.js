import React, { useState } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Table from "react-bootstrap/Table";
import { useSelector } from "react-redux";
import { isEmpty } from "./Utils";
const Panier = () => {
  // recuperation des id des produits
  const paniers = useSelector((state) => state.panier);
  const [nombre, setNombre] = useState(0);

  if (nombre < 0) {
    setNombre(0);
  }

  const incr = (nb) => {
    nb++;
    setNombre(nb);
  };
  const decr = (nb) => {
    nb--;
    setNombre(nb);
  };

  const supprimerObjetsDuplicats = (tableau) => {
    const idsExistants = [];
    return tableau.filter((objet) => {
      if (!idsExistants.includes(objet.id)) {
        idsExistants.push(objet.id);
        return true;
      }
      return false;
    });
  };

  const newTab = supprimerObjetsDuplicats(paniers);

  return (
    <Container className="mt-5">
      <Row>
        <Col>
          <h1>Mon Panier {newTab.length}</h1>
          {!isEmpty(newTab) &&
            newTab.map((panier, index) => (
              <div className="d-flex justify-content-around mt-5" key={index}>
                {/* <p>
                  {panier.name} <br />
                  <span className="lead">prix : {panier.price}</span>
                </p>
                <p>
                  <button
                    onClick={() => {
                      incr(nombre);
                    }}
                    className="me-3 px-3 btn"
                  >
                    +
                  </button>
                  {nombre}
                  <button
                    onClick={() => {
                      decr(nombre);
                    }}
                    className="ms-3 px-3 btn"
                  >
                    -
                  </button>
                </p> */}
                <Row className="mb-3">
                  <Form.Group as={Col} controlId="movieName">
                    <Form.Label>
                      {panier.name} <br />
                      <span>
                        prix du produit : <i>{panier.price}</i>
                      </span>
                    </Form.Label>
                  </Form.Group>

                  <Form.Group as={Col} controlId="noteMovie">
                    <Form.Control
                      type="number"
                      name="quantity"
                      placeholder="quantity"
                      max={panier.quantity}
                      min={0}
                    />
                    <span className="lead">
                      la quantite maximale est de
                      <span className="text-danger"> {panier.quantity}</span>
                    </span>
                  </Form.Group>
                </Row>
              </div>
            ))}
        </Col>
      </Row>
    </Container>
  );
};

export default Panier;
