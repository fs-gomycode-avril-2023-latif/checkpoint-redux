import React, { useState } from "react";
import "../assets/styles/navigation.css";
import { NavLink, Outlet } from "react-router-dom";
import lg from "../assets/images/OIP.png";
import { FaHome } from "react-icons/fa";
import { FaUserAlt } from "react-icons/fa";
import { FaShoppingBasket } from "react-icons/fa";
import { AiOutlineShoppingCart } from "react-icons/ai";

const Navigation = () => {
  const [toggleMenu, setToggleMenu] = useState(true);
  const toggleNavSmallScreen = () => {
    setToggleMenu(!toggleMenu);
  };
  return (
    <>
      <nav>
        <div id="logo">
          <img src={lg} width={20} height={20} alt="" />
          <p>Mon Magasin</p>
        </div>
        <div id="form">
          <form action="">
            <input type="text" placeholder="search product" />
            {/* <button className="btn btn-primary">S{FaSearch}</button> */}
          </form>
        </div>
        <div id="navigation">
          {toggleMenu && (
            <ul className="liste">
              <NavLink
                className={(nav) =>
                  nav.isActive ? "items nav-active" : "items"
                }
                to="/"
              >
                <li>
                  <p className="icon">
                    <FaHome />
                  </p>
                  <p className="titleIcon">Home</p>
                </li>
              </NavLink>
              <NavLink
                className={(nav) =>
                  nav.isActive ? "items nav-active" : "items"
                }
                to="/products"
              >
                <li>
                  <p className="icon">
                    <FaShoppingBasket />
                  </p>
                  <p className="titleIcon">Products</p>
                </li>
              </NavLink>
              <NavLink className="items" to="/users">
                <li>
                  <p className="icon">
                    <FaUserAlt />
                  </p>
                  <p className="titleIcon">Users</p>
                </li>
              </NavLink>
              <NavLink className="items" to="/panier">
                <li>
                  <p className="icon">
                    <AiOutlineShoppingCart />
                  </p>
                  <p className="titleIcon">Panier</p>
                </li>
              </NavLink>
            </ul>
          )}
          <button
            id="btn"
            onClick={toggleNavSmallScreen}
            className="btn btn-warning text-light"
          >
            Btn
          </button>
        </div>
      </nav>
      <Outlet />
    </>
  );
};

export default Navigation;
