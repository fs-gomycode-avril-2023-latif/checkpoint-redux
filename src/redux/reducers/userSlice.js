import { createSlice } from "@reduxjs/toolkit";

const userSlice = createSlice({
  name: "user",
  initialState: [],
  reducers: {
    addUser: (state, action) => {
      state.push(action.payload);
    },
    removeUser: (state, action) => {
      const userId = action.payload;
      return state.filter((product) => product.id !== userId);
    },
  },
});
export const { addUser, removeUser } = userSlice.actions;
export default userSlice.reducer;
