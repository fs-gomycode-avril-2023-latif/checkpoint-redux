import React from "react";
import { AiFillPlusCircle } from "react-icons/ai";
import { Link } from "react-router-dom";
import { useState } from "react";
import Modal from "react-bootstrap/Modal";
import FormUsers from "./FormUsers";
import Carte from "./Carte";
import { isEmpty } from "../Utils";
import { useSelector } from "react-redux";

const ListUsers = () => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const users = useSelector((state) => state.user);

  return (
    <div className="container mt-5">
      <div className="d-flex justify-content-between align-items-center">
        <h1>Liste des utilisateurs</h1>
        <div>
          <button className="btn btn-warning me-3">
            <Link className="lien" onClick={handleShow}>
              <AiFillPlusCircle /> <span className="ms-1">Add User</span>
            </Link>
          </button>
          <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
              <Modal.Title className="text-warning">Add User</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <FormUsers />
            </Modal.Body>
          </Modal>
        </div>
      </div>
      <div className="mt-4  d-flex ">
        {!isEmpty(users) &&
          users.map((user, index) => <Carte key={index} user={user} />)}
        {isEmpty(users) && (
          <p className="text-center lead ">Aucun utilisateurs </p>
        )}
      </div>
    </div>
  );
};

export default ListUsers;
