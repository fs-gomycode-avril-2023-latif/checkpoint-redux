import { configureStore } from "@reduxjs/toolkit";
import productSlice from "./reducers/productSlice";
import panierSlice from "./reducers/panierSlice";
import userSlice from "./reducers/userSlice";

export const store = configureStore({
  reducer: {
    product: productSlice,
    panier: panierSlice,
    user: userSlice,
  },
});
