import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
// import axios from "axios";

const Home = () => {
  // const data = axios
  //   .get("http://localhost:3000/products")
  //   .then((r) => console.log(r.data));
  return (
    <div>
      <Container fluid className="pt-5" style={{ backgroundColor: "orange" }}>
        <Row>
          <Col>
            <h3>Mon Magasin</h3>
            <p className="lead">
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Perspiciatis excepturi architecto accusamus sapiente ea commodi
              vero incidunt, molestias quae voluptas dolores vel voluptate
              optio, enim corrupti numquam eum voluptatem perferendis velit
              omnis ipsa illum aspernatur nesciunt molestiae? Quos debitis
              officiis in blanditiis tempora, exercitationem commodi eius
              similique minima. Obcaecati corrupti placeat sit autem nam rerum,
              culpa a eveniet atque nihil. Pariatur doloremque fugiat optio,
              sunt beatae voluptatibus obcaecati officia quisquam ducimus alias,
              iure quas magnam nobis odit porro, aliquam consequuntur harum
              maxime repellendus quaerat. Veritatis doloremque labore
              praesentium nihil aspernatur asperiores voluptas alias, ratione
              recusandae eum nisi cupiditate reprehenderit eaque!
            </p>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Home;
