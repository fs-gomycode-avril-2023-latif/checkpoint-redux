import React, { useState } from "react";

import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";

import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import { useDispatch } from "react-redux";
import { addProduct } from "../../redux/reducers/productSlice";
import { useNavigate } from "react-router-dom";

const FormAjout = () => {
  const [formData, setFormData] = useState({
    id: Date.now(),
    name: "",
    price: "",
    quantity: "",
    description: "",
    image: "",
  });
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleImageChange = (e) => {
    const file = e.target.files[0];
    const reader = new FileReader();
    reader.onload = () => {
      const imageData = reader.result;
      setFormData({ ...formData, image: imageData });
    };
    reader.readAsDataURL(file);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(addProduct(formData));
    setFormData({
      name: "",
      price: "",
      quantity: "",
      description: "",
      image: "",
    });
    navigate("/");
  };

  return (
    <Container fluid="md">
      <Row className="">
        <Col>
          <Form onSubmit={handleSubmit}>
            <Row className="mb-3">
              <Form.Group as={Col} controlId="movieName">
                <Form.Label>Nom Product</Form.Label>
                <Form.Control
                  type="text"
                  name="name"
                  placeholder="Nom Product"
                  value={formData.name}
                  onChange={handleChange}
                />
              </Form.Group>

              <Form.Group as={Col} controlId="noteMovie">
                <Form.Label>Quantity</Form.Label>
                <Form.Control
                  type="number"
                  name="quantity"
                  placeholder="quantity"
                  value={formData.quantity}
                  onChange={handleChange}
                />
              </Form.Group>
            </Row>
            <Row className="mb-3">
              <Form.Group as={Col} controlId="noteMovie">
                <Form.Label>Price</Form.Label>
                <Form.Control
                  type="number"
                  value={formData.price}
                  onChange={handleChange}
                  name="price"
                  placeholder="price"
                />
              </Form.Group>
            </Row>

            <Form.Group className="mb-3" controlId="movieDescription">
              <Form.Label>Description</Form.Label>
              <Form.Control
                as="textarea"
                value={formData.description}
                onChange={handleChange}
                name="description"
                rows={3}
              />
            </Form.Group>

            <Form.Group controlId="formFile" className="mb-3">
              <Form.Label>Image du film</Form.Label>
              <Form.Control
                type="file"
                name="image"
                onChange={handleImageChange}
              />
            </Form.Group>

            {/* <Form.Group className="mb-3" id="formGridCheckbox">
        <Form.Check type="checkbox" label="Check me out" />
      </Form.Group> */}

            <Button
              variant="warning"
              className="d-flex justify-content-end mt-5 text-white pull-right"
              type="submit"
            >
              Submit
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};

export default FormAjout;
