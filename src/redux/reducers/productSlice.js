// productSlice.js
import { createSlice } from "@reduxjs/toolkit";

const productSlice = createSlice({
  name: "product",
  initialState: [
    {
      name: "produit 1",
      quantity: "20",
      price: "2000",
      description: "jhj",
      image: "default.png",
      id: "PLKxO2y",
    },
    {
      name: "produit 2",
      quantity: "9",
      price: "9000",
      description: "lkjdfz sdhds",
      image: "default.png",
      id: "O3uobEw",
    },
  ],
  reducers: {
    addProduct: (state, action) => {
      state.push(action.payload);
    },
    removeProduct: (state, action) => {
      const productId = action.payload;
      return state.filter((product) => product.id !== productId);
    },
    updateProduct: (state, action) => {
      const { name, quantity, price, description, image, id } = action.payload;
      const produit = state.find((elt) => elt.id == id);
      if (produit) {
        produit.name = name;
        produit.quantity = quantity;
        produit.price = price;
        produit.description = description;
        produit.image = image;
      }
    },
  },
});

export const { addProduct, removeProduct, updateProduct } =
  productSlice.actions;
export default productSlice.reducer;
