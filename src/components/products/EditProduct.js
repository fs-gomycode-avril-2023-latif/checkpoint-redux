import React, { useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import { useDispatch, useSelector } from "react-redux";
import { updateProduct } from "../../redux/reducers/productSlice";

const EditProduct = () => {
  const { id } = useParams();
  const products = useSelector((state) => state.product);
  const existingProduct = products.filter((product) => product.id == id);
  const { name, quantity, price, description, image } = existingProduct[0];

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [data, setData] = useState({
    id: id,
    name: name,
    quantity: quantity,
    price: price,
    description: description,
    image: image,
  });

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };
  const handleImageChange = (e) => {
    const file = e.target.files[0];
    const reader = new FileReader();
    reader.onload = () => {
      const imageData = reader.result;
      setData({ ...data, image: imageData });
    };
    reader.readAsDataURL(file);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(data);
    dispatch(updateProduct(data));
    navigate("/products");
  };

  return (
    <div>
      <Container fluid="md">
        <Row className="">
          <Col>
            <Form onSubmit={handleSubmit}>
              <Row className="mb-3">
                <Form.Group as={Col} controlId="movieName">
                  <Form.Label>Nom Product</Form.Label>
                  <Form.Control
                    type="text"
                    name="name"
                    defaultValue={name}
                    onChange={handleChange}
                    placeholder="Nom Product"
                  />
                </Form.Group>

                <Form.Group as={Col} controlId="noteMovie">
                  <Form.Label>Quantity</Form.Label>
                  <Form.Control
                    type="number"
                    defaultValue={quantity}
                    onChange={handleChange}
                    name="quantity"
                    placeholder="quantity"
                  />
                </Form.Group>
              </Row>
              <Row className="mb-3">
                <Form.Group as={Col} controlId="noteMovie">
                  <Form.Label>Price</Form.Label>
                  <Form.Control
                    type="number"
                    name="price"
                    defaultValue={price}
                    onChange={handleChange}
                    placeholder="price"
                  />
                </Form.Group>
              </Row>

              <Form.Group className="mb-3" controlId="movieDescription">
                <Form.Label>Description</Form.Label>
                <Form.Control
                  as="textarea"
                  name="description"
                  defaultValue={description}
                  onChange={handleChange}
                  rows={3}
                />
              </Form.Group>

              <Form.Group
                controlId="formFile"
                onChange={handleImageChange}
                className="mb-3"
              >
                <Form.Label>Image du film</Form.Label>
                <Form.Control type="file" name="image" />
              </Form.Group>

              {/* <Form.Group className="mb-3" id="formGridCheckbox">
        <Form.Check type="checkbox" label="Check me out" />
      </Form.Group> */}

              <Button
                variant="warning"
                className="d-flex justify-content-end mt-5 text-white pull-right"
                type="submit"
              >
                Submit
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default EditProduct;
