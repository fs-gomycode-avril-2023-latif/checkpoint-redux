import React from "react";
import { BrowserRouter } from "react-router-dom";
import { Route, Routes } from "react-router-dom";
import Navigation from "./components/Navigation";
import Home from "./components/Home";
// products
import ListProducts from "./components/products/ListProducts";
import FormProducts from "./components/products/EditProduct";
// users
import ListUsers from "./components/users/ListUsers";
import FormUsers from "./components/users/FormUsers";
import Panier from "./components/Panier";

const App = () => {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Navigation />}>
            <Route index element={<Home />}></Route>
            <Route path="products">
              <Route index element={<ListProducts />}></Route>
              <Route path="editProduct/:id" element={<FormProducts />}></Route>
              <Route path="formulaire" element={<FormProducts />}></Route>
            </Route>
            <Route path="users">
              <Route index element={<ListUsers />}></Route>
              <Route path="formulaire" element={<FormUsers />}></Route>
            </Route>
            <Route path="panier" element={<Panier />}></Route>
          </Route>
        </Routes>
      </BrowserRouter>
    </>
  );
};

export default App;
