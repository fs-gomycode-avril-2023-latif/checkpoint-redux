// cartSlice.js
import { createSlice } from "@reduxjs/toolkit";

const panierSlice = createSlice({
  name: "panier",
  initialState: [],
  reducers: {
    addPanier: (state, action) => {
      const panier = action.payload;
      state.push(panier);
    },
    removeFromCart: (state, action) => {
      const productId = action.payload;
      return state.filter((product) => product.id !== productId);
    },
    clearCart: (state) => {
      return [];
    },
  },
});

export const { addPanier, removeFromCart, clearCart } = panierSlice.actions;
export default panierSlice.reducer;
