import React from "react";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { addUser } from "../../redux/reducers/userSlice";

const FormUsers = () => {
  const [formData, setFormData] = useState({
    fullName: "",
    phone: null,
    email: "",
    adress: "",
    id: Date.now(),
  });

  const dispatch = useDispatch();

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  // const handleImageChange = (e) => {
  //   const file = e.target.files[0];
  //   const reader = new FileReader();
  //   reader.onload = () => {
  //     const imageData = reader.result;
  //     setFormData({ ...formData, image: imageData });
  //   };
  //   reader.readAsDataURL(file);
  // };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(formData);
    dispatch(addUser(formData));

    setFormData({
      fullName: "",
      phone: null,
      email: "",
      adress: "",
      id: null,
    });
  };
  return (
    <Container fluid="md">
      <Row className="">
        <Col>
          <Form onSubmit={handleSubmit}>
            <Row className="mb-3">
              <Form.Group as={Col} controlId="movieName">
                <Form.Label>Nom et Prenoms</Form.Label>
                <Form.Control
                  type="text"
                  name="fullName"
                  placeholder="Nom Product"
                  value={formData.fullName}
                  onChange={handleChange}
                />
              </Form.Group>

              <Form.Group as={Col} controlId="noteMovie">
                <Form.Label>telephone</Form.Label>
                <Form.Control
                  type="text"
                  name="phone"
                  placeholder="phone"
                  value={formData.phone}
                  onChange={handleChange}
                />
              </Form.Group>
            </Row>
            <Row className="mb-3">
              <Form.Group as={Col} controlId="noteMovie">
                <Form.Label>email</Form.Label>
                <Form.Control
                  type="text"
                  value={formData.email}
                  onChange={handleChange}
                  name="email"
                  placeholder="email"
                />
              </Form.Group>
            </Row>
            <Row className="mb-3">
              <Form.Group as={Col} controlId="noteMovie">
                <Form.Label>Adresse</Form.Label>
                <Form.Control
                  type="text"
                  value={formData.adress}
                  onChange={handleChange}
                  name="adress"
                  placeholder="adresse"
                />
              </Form.Group>
            </Row>

            {/* <Form.Group controlId="formFile" className="mb-3">
              <Form.Label>Image du film</Form.Label>
              <Form.Control
                type="file"
                name="image"
                onChange={handleImageChange}
              />
            </Form.Group> */}

            {/* <Form.Group className="mb-3" id="formGridCheckbox">
        <Form.Check type="checkbox" label="Check me out" />
      </Form.Group> */}

            <Button
              variant="warning"
              className="d-flex justify-content-end mt-5 text-white pull-right"
              type="submit"
            >
              Submit
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};

export default FormUsers;
