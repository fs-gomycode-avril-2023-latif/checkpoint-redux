import React from "react";
import Table from "react-bootstrap/Table";
import { AiFillPlusCircle } from "react-icons/ai";
import { Link, Navigate, useNavigate } from "react-router-dom";
import "../../assets/styles/products.css";
import { AiOutlineShoppingCart } from "react-icons/ai";
import { AiFillEdit } from "react-icons/ai";
import { AiOutlineDelete } from "react-icons/ai";

import { useState } from "react";
// import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import FormAjout from "./FormAjout";
import { isEmpty } from "../Utils";
import { useDispatch, useSelector } from "react-redux";
import { removeProduct } from "../../redux/reducers/productSlice";
import { addPanier } from "../../redux/reducers/panierSlice";

const ListProducts = () => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  // products
  const products = useSelector((state) => state.product);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  // PANIER
  const handlePanier = (product) => {
    alert("produit ajouté au panier");
    dispatch(addPanier(product));
    navigate("/panier");
  };

  console.log(products.length);

  return (
    <div className="container mt-5">
      <div className="d-flex justify-content-between align-items-center">
        <h1>Liste des produits</h1>
        <div>
          <button className="btn btn-warning me-3">
            <Link className="lien" onClick={handleShow}>
              <AiFillPlusCircle /> <span className="ms-1">Add Products</span>
            </Link>
          </button>
          <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
              <Modal.Title className="text-warning">Add Product</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <FormAjout />
            </Modal.Body>
            {/* <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
              <Button variant="primary" onClick={handleClose}>
                Save Changes
              </Button>
            </Modal.Footer> */}
          </Modal>
        </div>
      </div>
      <Table striped bordered hover className="mt-4 text-center" size="sm">
        {!isEmpty(products) && (
          <thead>
            <tr>
              <th>id</th>
              <th>Name products</th>
              <th>Image</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Description</th>
              <th>actions</th>
            </tr>
          </thead>
        )}

        <tbody>
          {!isEmpty(products) &&
            products.map((product, index) => (
              <tr key={index}>
                <td>{product.id}</td>
                <td>{product.name}</td>
                <td>
                  <img
                    src={product.image}
                    width={50}
                    height={50}
                    alt={`photo de ${product.name}`}
                  />
                </td>
                <td>{product.price}</td>
                <td>{product.quantity}</td>
                <td>{product.description}</td>
                <td>
                  <span
                    onClick={() => {
                      handlePanier(product);
                    }}
                    className="me-3 fs-3 lienIcon"
                  >
                    <AiOutlineShoppingCart />
                  </span>
                  <Link
                    to={`editProduct/${product.id}`}
                    className="me-3 fs-3 lienIcon"
                  >
                    <AiFillEdit />
                  </Link>
                  <Link
                    to="#"
                    onClick={() => {
                      alert("produit supprimé");
                      dispatch(removeProduct(product.id));
                    }}
                    className="me-3 fs-3 lienIcon"
                  >
                    <AiOutlineDelete />
                  </Link>
                </td>
              </tr>
            ))}
        </tbody>
      </Table>

      {isEmpty(products) && (
        <p className="text-center lead mt-5">Aucun produit disponible</p>
      )}
    </div>
  );
};

export default ListProducts;
